<?php

class Connection{
    private $conn;

    public function __construct()
    {
        $coonString = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";.DB_CHARSET.";
    

    try{
        $this->conn = new PDO($coonString, DB_USER, DB_PASSWORD);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION);
    }catch(PDOException $e){
        $this->conn = "Koneksi Error";
        echo "Error: " . $e->getMessage();
    }
}

public function connect()
{
    return $this->conn;
}
}