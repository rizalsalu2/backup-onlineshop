<?php
 
   class Product extends Controllers{
       public function __construct()
       {
           parent::__construct();
       }

       public function product($params)
       {
           $data['tag_page'] = "Product";
           $data['page_title'] = "Daftar Product";
           $data['page_name'] = "Product";
           $this->views->getView($this, "Product", $data);
       }
   }