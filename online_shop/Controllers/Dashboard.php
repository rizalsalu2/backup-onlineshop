<?php
 
   class Dashboard extends Controllers{

    // function untuk memanggil attribute dan method class controllers
       public function __construct()
       {
           parent::__construct();
       }

    // function untuk mengarahkan ke halaman dasboard
       public function dashboard($params)
       {
           $data['tag_page'] = "Dashboard Admin";
           $data['page_title'] = "Dashboard Admin";
           $data['page_name'] = "dashboard";
           $this->views->getView($this, "dashboard", $data);
       }
   }
