<?php 

   function base_url()
   {
       return BASE_URL;
   }

   function assetAdmin()
   {
      return BASE_URL."Assets/admin/";       
   }

   function headerAdmin($data="")
   {
       $view_header = "Views/layout/header_admin.php";
       require_once($view_header);
   }

   function footerAdmin()
   {
       $view_footer = "Views/layout/footer_admin.php";
       require_once($view_footer);
   }