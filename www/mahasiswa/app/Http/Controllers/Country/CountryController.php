<?php

namespace App\Http\Controllers\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CountryModel;

class CountryController extends Controller
{
    public function country(){
      return response()->json(CountryModel::get(), 199);
    }
}
