<?php

class Connection {
    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $db = "siswa";
    private $connect;

    public function __construct()
    {
        $connString = "mysql:host=".$this->host.";dbname=".
        $this->db.";charset=utf8";
        try{
            $this->connect = new PDO($connString,$this->user,
            $this->password);
            $this->connect->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);
            // echo "koneksi berhasil";
        } catch (Exception $e) {
            $this->connect = "koneksi Error";
            echo "ERROR: ". $e->getMessage();
        }
    }

    public function Connection()
    {
        return $this->connect;
    }
}

// $connect = new Connection();