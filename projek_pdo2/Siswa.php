<?php

class Siswa extends Connection{
    private $nisn;
    private $nama;
    private $agama;
    private $alamat;
    private $kelas;
    private $no_telp;
    private $conn;

    public function __construct()
    {
        $this->conn = new Connection();
        $this->conn = $this->conn->connection();
    }
   public function insertSiswa(int $nisn, string $strnama, string $stragama, string $stralamat, string $strkelas, string $strno_telp)
   {
       $this->nisn = $nisn;
       $this->nama = $strnama;
       $this->agama = $stragama;
       $this->alamat = $stralamat;
       $this->kelas = $strkelas;
       $this->no_telp = $strno_telp;
      

       $sql = "INSERT INTO Siswa(nisn,nama,agama,alamat,kelas,no_telp) VALUES(?,?,?,?,?,?)";

       $insert = $this->conn->prepare($sql);

       $arrData = array($this->nisn,$this->nama,$this->agama,$this->alamat,$this->kelas,$this->no_telp);

       $resInsert = $insert->execute($arrData);

       $lastId = $this->conn->lastInsertId();
       return $lastId;
   }
   public function getSiswa()
   {
       $sql = "SELECT * FROM Siswa";
       $get = $this->conn->query($sql);
       $resGet = $get->fetchall(PDO::FETCH_ASSOC);
       return $resGet;
   }
   public function updateSiswa(int $id, int $nisn, string $strnama, string $stragama, string $stralamat, string $strkelas, string $strno_telp)
   {
    $this->nisn = $nisn;
    $this->nama = $strnama;
    $this->agama = $stragama;
    $this->alamat = $stralamat;
    $this->kelas = $strkelas;
    $this->no_telp = $strno_telp;

    $sql = "UPDATE siswa set nisn=?, nama=?, agama=?, alamat=?, kelas=?, no_telp=? WHERE id=$id";

    $update = $this->conn->prepare($sql);

    $arrData = array($this->nisn,$this->nama,$this->agama,$this->alamat,$this->kelas,$this->no_telp);

    $resUpdate = $update->execute($arrData);

    return $resUpdate;
}

public function getUpdate(int $id)
{
$sql = "SELECT * FROM Siswa WHERE id = ? ";

$query = $this->conn->prepare($sql);

$arrwhere = array($id);

$query->execute($arrwhere);

$request = $query->fetch(PDO::FETCH_ASSOC);

return $request;
}

 public function deleteSiswa(int $id)
 {
   $sql = "DELETE FROM Siswa WHERE id = ?";

   $delete = $this->conn->prepare($sql);

   $arrwhere = array($id);

   $del = $delete->execute($arrwhere);

   return $del;
 }
}

